# -*- coding: utf-8 -*-

import unittest
import datetime

from pyramid import testing


####################
#   Testy modeli   #
####################

class BlogModelTests(unittest.TestCase):

    def _getTargetClass(self):
        from .models import Blog
        return Blog

    def _makeOne(self):
        return self._getTargetClass()()

    def test_it(self):
        blog = self._makeOne()
        self.assertEqual(blog.__parent__,None)
        self.assertEqual(blog.__name__,None)

class PostModelTests(unittest.TestCase):

    def _getTargetClass(self):
        from .models import Post
        return Post

    def _makeOne(self, title=u'some title', content=u'some content', created=datetime.datetime.now()):
        return self._getTargetClass()(title=title, content=content, created=created)

    def test_constructor(self):
        instance = self._makeOne()
        self.assertEqual(instance.title, u'some title')
        self.assertEqual(instance.content, u'some content')

class AppmakerTests(unittest.TestCase):

    def _callFUT(self, zodb_root):
        from .models import appmaker
        return appmaker(zodb_root)

    def test_it(self):
        root = {}
        self._callFUT(root)
        self.assertEqual(root['app_root']['Inicjalizacja'].title, 'Inicjalizacja')

#####################
#   Testy widoków   #
#####################

class ViewBlogTests(unittest.TestCase):
    def test_it(self):
        from .views import view_blog
        context = testing.DummyResource()
        request = testing.DummyRequest()
        response = view_blog(context, request)
        self.assertEqual(response.location, 'http://194.29.175.240/~5/mysite.wsgi')

class ViewPostTests(unittest.TestCase):
    def _callFUT(self, context, request):
        from .views import view_post
        return view_post(context, request)

class AddPost(unittest.TestCase):
    def _callFUT(self, context, request):
        from .views import add_post
        return add_post(context, request)

    def test_it_submitted(self):
        context = testing.DummyResource()
        data = {'form.submitted':True, 'body':'Dziala jak marzenie'}
        request = testing.DummyRequest(data, post=data)
        request.subpath = ['OtherPage']
        self._callFUT(context, request)
        page = context['OtherPage']
        self.assertEqual(page.data, 'Dziala jak marzenie')
        self.assertEqual(page.__name__, 'OtherPage')
        self.assertEqual(page.__parent__, context)
