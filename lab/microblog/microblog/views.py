# -*- coding: utf-8 -*-
import datetime

from docutils.core import publish_parts
import re

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from .models import Blog, Post, Comment


# Glowna
@view_config(context='.models.Blog', renderer='templates/list.pt')
def view_blog(context, request):
    """ Wyswietla szczegolowo dany wpis """
    blog_list=context.items()
    blog_list= sorted(blog_list, key=lambda tup: tup[1].created, reverse=True)
    page = 1
    if request.method == 'POST':
        try:
            page = int(request.params['page'])
        except:
            pass
    posts = list()
    for item in blog_list[(page-1)*10:(page*10-1)]:
        try:
            posts.append(item)
        except:
            pass
    numbers  ={1:1}
    for post in posts:
        numbers[post]=1
        for comment in post[1].comment:
            numbers[post]+=1
    pages = list()
    for x in range (0,len(blog_list)/10+1):
        pages.append(x+1)

    return {'blog':posts, 'add_url':add_post(context, request),'numbers':numbers,'pages':pages}

# Szczegoly
@view_config(context='.models.Post', renderer='templates/view.pt')
def view_post(context, request):
    """ Wyswietla liste n ostatnich wpisow """
    post = context
    return {'post':post,'add_comment':add_comment(context, request)}

# Komentarz
@view_config(context='.models.Comment',renderer='templates/comment.pt',)
def view_comment(context,request):
    pass


@view_config(name='add', context='.models.Blog')
def add_post(context, request):
    if request.method == 'POST':
        try:
            title = request.POST['title']
            content = request.POST['content']
            post = Post(title, content, datetime.datetime.now())
            context[title] = post
            return HTTPFound(location=request.resource_url(context))
        except:
            pass

@view_config(name='comment', context='.models.Post')
def add_comment(context,request):
    if request.method == 'POST':
        comment = Comment(1,request.params['comment'])
        context.addComment(comment)



