import os, sys

activate_this = '/home/p5/pyrenv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

from pyramid.paster import get_app, setup_logging
ini_path = '/home/p5/lab/microblog/development.ini'
setup_logging(ini_path)
application = get_app(ini_path, 'main')